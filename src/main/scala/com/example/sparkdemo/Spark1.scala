package com.example.sparkdemo

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * 创建rdd
 *
 * @author zhang.t.c
 * @date 2021/5/10
 */
object Spark1 {

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("Spark1")
    val sc = new SparkContext(sparkConf)

    val lines: RDD[String] = sc.textFile("D:\\demo\\spark-demo\\datas\\wordcount")
    val res: Array[(String, Int)] = lines
      .flatMap(_.split(" "))
      .map((_, 1))
      .reduceByKey(_ + _)
      .collect()

    res.foreach(println)

    sc.stop()
  }




}
