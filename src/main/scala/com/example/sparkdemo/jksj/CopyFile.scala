package com.example.sparkdemo.jksj

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 *
 *
 * @author zhang.t.c
 * @date 2021/8/23
 */
object CopyFile {

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("Spark1")
    val sc = new SparkContext(sparkConf)

    val files: RDD[(String, String)] = sc.wholeTextFiles("D:\\demo\\spark-demo\\datas\\copyfiles")
    files.foreach(println)
//    files.mapPartitions(p => {
//      p.
//    })

    sc.stop()
  }

}
