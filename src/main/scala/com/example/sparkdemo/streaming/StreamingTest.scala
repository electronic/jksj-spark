package com.example.sparkdemo.streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
 *
 *
 * @author zhang.t.c
 * @date 2021/8/19
 */
object StreamingTest {

  def main(args: Array[String]): Unit = {
    val sc: SparkConf = new SparkConf().setMaster("local[*]").setAppName("StreamingTest")
    val ssc = new StreamingContext(sc, Seconds(3))

    val line: ReceiverInputDStream[String] = ssc.socketTextStream("hadoop112", 9999)

    val word: DStream[String] = line.flatMap(_.split(" "))

    val wordcount: DStream[(String, Int)] = word.map((_, 1)).reduceByKey(_ + _)

    wordcount.print()

    ssc.start()
    ssc.awaitTermination()

  }

}
